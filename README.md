# vtl README

VTL transformation schemes highlighter developed by meaningfulData.

## Features

Highlights VTL transformation schemes

## Requirements

No requirements

## Extension Settings

No settings required

## Known Issues

Symbol operators (like +, <> etc not working)

## Release Notes

First release

### 1.0.0

Initial release of ...
